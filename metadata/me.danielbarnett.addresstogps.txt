AntiFeatures:NonFreeNet
Categories:Navigation
License:MIT
Web Site:http://addresstogps.com/
Source Code:https://github.com/DanielBarnett714/AddressToGPS
Issue Tracker:https://github.com/DanielBarnett714/AddressToGPS/issues
Donate:http://addresstogps.com/

Auto Name:AddressToGPS
Summary:A simple address/POI search for navigation apps
Description:
Search for an address, location, or area using the Google Maps javascript API
via [http://addresstogps.com/ AddressToGPS.com], a server provided by the
original author. Once you have selected a location you then can get the
coordinates and open the locationin a maps app (OsmAnd). This tool does not
require your location, only an internet connection.
.

Repo Type:git
Repo:https://github.com/DanielBarnett714/AddressToGPS

Build:1.0,1
    commit=v1.05
    subdir=app
    gradle=yes

Build:1.09,9
    commit=1.09
    subdir=app
    gradle=yes

Build:1.1,10
    disable=package id changed
    commit=1.1
    subdir=app
    gradle=yes

Build:1.11,11
    commit=1.11
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.11
Current Version Code:11
